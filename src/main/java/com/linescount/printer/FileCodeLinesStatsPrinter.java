package com.linescount.printer;

import com.linescount.LinesCountApplication;
import com.linescount.file.IFileCodeLinesStatsBuilder;
import com.linescount.stringifier.IFileTreeStringifier;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class FileCodeLinesStatsPrinter implements IFileCodeLinesStatsPrinter {

    private IFileTreeStringifier stringifier = LinesCountApplication.getComponentsFactory().getTreeStringifier();
    private IFileCodeLinesStatsBuilder statsBuilder = LinesCountApplication.getComponentsFactory()
            .getFileCodeLinesStatsBuilder();

    public void printFileCodeLinesStats(String filePath, PrintStream out) throws IOException {
        File file = new File(filePath);
        out.println(stringifier.convertToString(statsBuilder.getFileCodeLinesTree(file)));
    }

}
