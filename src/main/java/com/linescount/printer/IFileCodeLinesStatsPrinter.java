package com.linescount.printer;

import java.io.IOException;
import java.io.PrintStream;

public interface IFileCodeLinesStatsPrinter {

    void printFileCodeLinesStats(String filePath, PrintStream outputStream) throws IOException;

}
