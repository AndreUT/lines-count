package com.linescount.string;

public interface IStringCodeLinesCounter {

    int getCodeLinesCount(String input);

}
