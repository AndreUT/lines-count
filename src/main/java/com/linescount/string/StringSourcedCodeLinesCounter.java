package com.linescount.string;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringSourcedCodeLinesCounter implements IStringCodeLinesCounter {

    private static final String LINE_SEPARATOR = "\n";
    private static final String LINE_COMMENT = "//";
    private static final String BLOCK_COMMENT_REGEX = "/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/";

    public int getCodeLinesCount(String input) {
        String inputWithoutBlockComments = input.replaceAll(BLOCK_COMMENT_REGEX, "");
        List<String> collected = Arrays.asList(inputWithoutBlockComments.split(LINE_SEPARATOR))
                .stream()
                .filter(line -> isCodeLine(line))
                .collect(Collectors.toList());
        return collected.size();
    }

    private boolean isCodeLine(String line) {
        return !isBlankLine(line) && !isStartsWithLineComment(line);
    }

    private boolean isBlankLine(String line) {
        return line.trim().length() == 0;
    }

    private boolean isStartsWithLineComment(String line) {
        return line.trim().startsWith(LINE_COMMENT);
    }

}
