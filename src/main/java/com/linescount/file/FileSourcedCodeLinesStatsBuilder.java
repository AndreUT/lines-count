package com.linescount.file;

import com.linescount.LinesCountApplication;
import com.linescount.dto.FileWithLinesTreeNode;
import com.linescount.string.IStringCodeLinesCounter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FileSourcedCodeLinesStatsBuilder implements IFileCodeLinesStatsBuilder {

    private final IStringCodeLinesCounter stringSourcedLinesCounter = LinesCountApplication.getComponentsFactory()
            .getStringLinesCounter();

    public FileWithLinesTreeNode getFileCodeLinesTree(File input) throws IOException {
        if (input.isFile()) {
            String fileContent = new String(Files.readAllBytes(Paths.get(input.getPath())));
            return new FileWithLinesTreeNode(input, stringSourcedLinesCounter.getCodeLinesCount(fileContent),
                    Collections.emptyList());
        } else {
            // input is a folder
            List<FileWithLinesTreeNode> children = new ArrayList<>();
            int folderLinesCount = 0;
            for (File child : Objects.requireNonNull(input.listFiles())) {
                FileWithLinesTreeNode childNode = getFileCodeLinesTree(child);
                children.add(childNode);
                folderLinesCount += childNode.getLinesCount();
            }
            return new FileWithLinesTreeNode(input, folderLinesCount, children);
        }
    }

}
