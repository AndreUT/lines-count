package com.linescount.file;

import com.linescount.dto.FileWithLinesTreeNode;

import java.io.File;
import java.io.IOException;

public interface IFileCodeLinesStatsBuilder {

    FileWithLinesTreeNode getFileCodeLinesTree(File input) throws IOException;

}
