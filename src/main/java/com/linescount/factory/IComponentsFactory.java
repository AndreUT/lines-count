package com.linescount.factory;

import com.linescount.file.IFileCodeLinesStatsBuilder;
import com.linescount.printer.FileCodeLinesStatsPrinter;
import com.linescount.string.IStringCodeLinesCounter;
import com.linescount.stringifier.IFileTreeStringifier;

public interface IComponentsFactory {

    IStringCodeLinesCounter getStringLinesCounter();

    IFileCodeLinesStatsBuilder getFileCodeLinesStatsBuilder();

    IFileTreeStringifier getTreeStringifier();

    FileCodeLinesStatsPrinter getStatsPrinter();
}
