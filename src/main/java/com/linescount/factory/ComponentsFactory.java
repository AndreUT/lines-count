package com.linescount.factory;

import com.linescount.file.FileSourcedCodeLinesStatsBuilder;
import com.linescount.file.IFileCodeLinesStatsBuilder;
import com.linescount.printer.FileCodeLinesStatsPrinter;
import com.linescount.string.IStringCodeLinesCounter;
import com.linescount.string.StringSourcedCodeLinesCounter;
import com.linescount.stringifier.FileWithLinesTreeStringifier;
import com.linescount.stringifier.IFileTreeStringifier;

public class ComponentsFactory implements IComponentsFactory {

    public IStringCodeLinesCounter getStringLinesCounter() {
        return new StringSourcedCodeLinesCounter();
    }

    public IFileCodeLinesStatsBuilder getFileCodeLinesStatsBuilder() {
        return new FileSourcedCodeLinesStatsBuilder();
    }

    public IFileTreeStringifier getTreeStringifier() {
        return new FileWithLinesTreeStringifier();
    }

    public FileCodeLinesStatsPrinter getStatsPrinter() {
        return new FileCodeLinesStatsPrinter();
    }

}
