package com.linescount;

import com.linescount.factory.ComponentsFactory;

import java.io.IOException;
import java.util.Scanner;

public class LinesCountApplication {

    private static final ComponentsFactory componentsFactory = new ComponentsFactory();

    public static ComponentsFactory getComponentsFactory() {
        return componentsFactory;
    }

    public static void main(String[] args) throws IOException {
        new LinesCountApplication().handleUserInput();
    }

    private void handleUserInput() throws IOException {
        System.out.println("Enter file path...");
        String filePath = getUserInput();
        componentsFactory.getStatsPrinter().printFileCodeLinesStats(filePath, System.out);
    }

    private String getUserInput() {
        Scanner inScanner = new Scanner(System.in);
        return inScanner.nextLine();
    }
}
