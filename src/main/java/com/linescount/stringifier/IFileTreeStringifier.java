package com.linescount.stringifier;

import com.linescount.dto.FileWithLinesTreeNode;

public interface IFileTreeStringifier {

    String convertToString(FileWithLinesTreeNode node);

}
