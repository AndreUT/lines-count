package com.linescount.stringifier;

import com.linescount.dto.FileWithLinesTreeNode;

import java.io.File;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FileWithLinesTreeStringifier implements IFileTreeStringifier {

    private static final String OUTPUT_FORMAT = "%s : %d \n";
    private static final String TAB_SYMBOL = "\t";

    public String convertToString(FileWithLinesTreeNode node) {
        return convertWithDepth(node, 0);
    }

    private String convertWithDepth(FileWithLinesTreeNode node, int depth) {
        StringBuilder result = new StringBuilder();
        File file = node.getFile();
        if (file.isFile()) {
            result.append(getTabsIndentation(depth));
            result.append(String.format(OUTPUT_FORMAT, file.getName(), node.getLinesCount()));
        } else {
            result.append(getTabsIndentation(depth));
            result.append(String.format(OUTPUT_FORMAT, file.getName(), node.getLinesCount()));
            for (FileWithLinesTreeNode childNode : node.getChildren()) {
                result.append(convertWithDepth(childNode, depth + 1));
            }
        }
        return result.toString();
    }

    private String getTabsIndentation(int depth) {
        return IntStream.range(0, depth).mapToObj(i -> TAB_SYMBOL).collect(Collectors.joining());
    }

}
