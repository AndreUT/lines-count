package com.linescount.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileWithLinesTreeNode {

    private final File file;
    private final int linesCount;

    private final List<FileWithLinesTreeNode> children;

    public FileWithLinesTreeNode(File file, int linesCount, List<FileWithLinesTreeNode> children) {
        this.file = file;
        this.linesCount = linesCount;
        this.children = new ArrayList<>(children);
    }

    public File getFile() {
        return new File(file.getPath());
    }

    public int getLinesCount() {
        return linesCount;
    }

    public List<FileWithLinesTreeNode> getChildren() {
        return new ArrayList<>(children);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileWithLinesTreeNode that = (FileWithLinesTreeNode) o;
        return linesCount == that.linesCount &&
                Objects.equals(file, that.file) &&
                Objects.equals(children, that.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(file, linesCount, children);
    }
}
