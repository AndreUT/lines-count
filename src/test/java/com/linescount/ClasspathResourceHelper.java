package com.linescount;

import java.io.File;

public class ClasspathResourceHelper {

    public File getClasspathResource(String path) {
        return new File(getClass().getClassLoader().getResource(path).getFile());
    }

}
