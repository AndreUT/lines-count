package com.linescount.printer;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FileCodeLinesStatsPrinterTest {

    private class InputSavingPrintStream extends PrintStream {

        private List<String> stringsToPrint = new ArrayList<>();

        private InputSavingPrintStream(OutputStream outputStream) {
            super(outputStream);
        }

        @Override
        public void println(String x) {
            stringsToPrint.add(x);
        }

        public List<String> getStringsToPrint() {
            return stringsToPrint;
        }
    }

    private static final String BRANCHED_FOLDER_PATH = "./src/test/resources/twoFoldersTest";
    private static final String EXPECTED_BRANCHED_NODE_STRINGIFIED_VALUE = "twoFoldersTest : 6 \n" +
            "\t1 : 1 \n" +
            "\t\t1.java : 1 \n" +
            "\t2 : 5 \n" +
            "\t\t2.java : 2 \n" +
            "\t\t3.java : 3 \n";

    private FileCodeLinesStatsPrinter printer;

    @Before
    public void initPrinter() {
        printer = new FileCodeLinesStatsPrinter();
    }

    @Test
    public void shouldPrintStringifiedFileLinesStatsToPrintStream() throws IOException {
        OutputStream outputStream = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                // do nothing
            }
        };
        InputSavingPrintStream mockedPrintStream = new InputSavingPrintStream(outputStream);

        printer.printFileCodeLinesStats(BRANCHED_FOLDER_PATH, mockedPrintStream);

        List<String> stringsToPrint = mockedPrintStream.getStringsToPrint();
        assertEquals(1, stringsToPrint.size());
        assertTrue(stringsToPrint.contains(EXPECTED_BRANCHED_NODE_STRINGIFIED_VALUE));
    }

}