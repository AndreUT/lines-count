package com.linescount.file;

import com.linescount.ClasspathResourceHelper;
import com.linescount.dto.FileWithLinesTreeNode;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class FileSourcedCodeLinesStatsBuilderTest {

    private static final String SINGLE_FILE_PATH = "singleFileTest/App.java";
    private static final int SINGLE_FILE_EXPECTED_LINES_COUNT = 5;

    private static final String BRANCHED_FOLDER_PATH = "twoFoldersTest";
    private static final String BRANCH1 = "twoFoldersTest/1";
    private static final String BRANCH2 = "twoFoldersTest/2";
    private static final String FILE1 = "twoFoldersTest/1/1.java";
    private static final String FILE2 = "twoFoldersTest/2/2.java";
    private static final String FILE3 = "twoFoldersTest/2/3.java";

    private ClasspathResourceHelper classpathResourceHelper = new ClasspathResourceHelper();
    private FileSourcedCodeLinesStatsBuilder fileSourcedCounter;

    @Before
    public void initFileSourcedCounter() {
        fileSourcedCounter = new FileSourcedCodeLinesStatsBuilder();
    }

    @Test
    public void shouldReturnFiveLinesDtoForSingleFileInput() throws IOException {
        File inputFile = classpathResourceHelper.getClasspathResource(SINGLE_FILE_PATH);

        FileWithLinesTreeNode expected = new FileWithLinesTreeNode(inputFile, SINGLE_FILE_EXPECTED_LINES_COUNT, new ArrayList<>());
        assertEquals(expected, fileSourcedCounter.getFileCodeLinesTree(inputFile));
    }

    @Test
    public void shouldReturnTwoBranchesFileTree() throws IOException {
        File input = classpathResourceHelper.getClasspathResource(BRANCHED_FOLDER_PATH);

        FileWithLinesTreeNode file1 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(FILE1),
                1,
                Collections.emptyList());
        FileWithLinesTreeNode file2 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(FILE2),
                2,
                Collections.emptyList());
        FileWithLinesTreeNode file3 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(FILE3),
                3,
                Collections.emptyList());

        FileWithLinesTreeNode branch1 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(BRANCH1),
                1,
                Collections.singletonList(file1));
        FileWithLinesTreeNode branch2 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(BRANCH2),
                5,
                Arrays.asList(file2, file3));

        FileWithLinesTreeNode root = new FileWithLinesTreeNode(input, 6, Arrays.asList(branch1, branch2));

        assertEquals(root, fileSourcedCounter.getFileCodeLinesTree(input));
    }

}