package com.linescount.stringifier;

import com.linescount.ClasspathResourceHelper;
import com.linescount.dto.FileWithLinesTreeNode;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class FileWithLinesTreeStringifierTest {

    private static final String SINGLE_FILE_PATH = "singleFileTest/App.java";
    private static final String BRANCHED_FOLDER_PATH = "twoFoldersTest";

    private static final String BRANCH1 = "twoFoldersTest/1";
    private static final String BRANCH2 = "twoFoldersTest/2";
    private static final String FILE1 = "twoFoldersTest/1/1.java";
    private static final String FILE2 = "twoFoldersTest/2/2.java";
    private static final String FILE3 = "twoFoldersTest/2/3.java";

    private static final String FILE_OUTPUT_FORMAT = "%s : %d \n";
    private static final String EXPECTED_BRANCHED_NODE_STRINGIFIED_VALUE = "twoFoldersTest : 6 \n" +
                    "\t1 : 1 \n" +
                    "\t\t1.java : 1 \n" +
                    "\t2 : 5 \n" +
                    "\t\t2.java : 2 \n" +
                    "\t\t3.java : 3 \n";

    private ClasspathResourceHelper classpathResourceHelper = new ClasspathResourceHelper();
    private FileWithLinesTreeStringifier fileTreeStringifier;

    @Before
    public void initFileTreeStringifier() {
        fileTreeStringifier = new FileWithLinesTreeStringifier();
    }

    @Test
    public void shouldReturnSingleLineStringForSingleFileInput() {
        File singleFile = classpathResourceHelper.getClasspathResource(SINGLE_FILE_PATH);
        FileWithLinesTreeNode node = new FileWithLinesTreeNode(singleFile, 5, Collections.emptyList());
        assertEquals(String.format(FILE_OUTPUT_FORMAT, singleFile.getName(), 5),
                fileTreeStringifier.convertToString(node));
    }

    @Test
    public void shouldReturnFoldersStructureForFolderInput() {
        File input = classpathResourceHelper.getClasspathResource(BRANCHED_FOLDER_PATH);

        FileWithLinesTreeNode file1 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(FILE1),
                1,
                Collections.emptyList());
        FileWithLinesTreeNode file2 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(FILE2),
                2,
                Collections.emptyList());
        FileWithLinesTreeNode file3 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(FILE3),
                3,
                Collections.emptyList());

        FileWithLinesTreeNode branch1 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(BRANCH1),
                1,
                Collections.singletonList(file1));
        FileWithLinesTreeNode branch2 = new FileWithLinesTreeNode(classpathResourceHelper.getClasspathResource(BRANCH2),
                5,
                Arrays.asList(file2, file3));

        FileWithLinesTreeNode root = new FileWithLinesTreeNode(input, 6, Arrays.asList(branch1, branch2));
        assertEquals(EXPECTED_BRANCHED_NODE_STRINGIFIED_VALUE,
                fileTreeStringifier.convertToString(root));
    }

}