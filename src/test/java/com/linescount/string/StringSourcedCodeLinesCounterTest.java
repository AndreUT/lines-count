package com.linescount.string;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringSourcedCodeLinesCounterTest {

    private StringSourcedCodeLinesCounter counter = new StringSourcedCodeLinesCounter();

    @Test
    public void shouldReturnZeroLinesForEmptyString() {
        String code = "";
        assertEquals(0, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnZeroLinesForSpaceSymbol() {
        String code = " ";
        assertEquals(0, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnZeroLinesForTabSymbol() {
        String code = "\t";
        assertEquals(0, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnZeroLinesForNewLineSymbol() {
        String code = "\n";
        assertEquals(0, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnZeroLinesForSingleLineComment() {
        String code = "//";
        assertEquals(0, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnZeroLinesForSingleLineCommentWithPrecedingSpace() {
        String code = " //";
        assertEquals(0, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnZeroLinesForSingleLineCommentWithPrecedingTab() {
        String code = "\t//";
        assertEquals(0, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnOneLineForSingleLineInput() {
        String code = "public class Main {}";
        assertEquals(1, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnOneLineForSingleLineInputFollowedWithLineComment() {
        String code = "public class Main {} //";
        assertEquals(1, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnTwoLinesForThreeLinedInputWithLineComment() {
        String code = "public class Main {\n" +
                "// someMethod()\n" +
                "}";
        assertEquals(2, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnTwoLinesForThreeLinedInputWithBlockComment() {
        String code = "public class Main {\n" +
                "/* someMethod() */\n" +
                "}";
        assertEquals(2, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnThreeLinesForThreeLinedInputWithBlockCommentStartingAfterCodeLineAndEndingBeforeCodeLine() {
        String code = "public class Main { \n" +
                "someMethod1();/*\n" +
                "some text inside of block comment\n" +
                "*/someMethod2();\n" +
                "}";
        assertEquals(3, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnThreeLinesForThreeLinedInputWithSeveralBlockCommentsInTheMiddleOfTheLine() {
        String code = "public class Main { \n" +
                "someObject./* comm1 */method1()/*comm2*/.method2(); \n" +
                "}";
        assertEquals(3, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnThreeLinesForThreeLinedInputWithSeveralBlockCommentInARow() {
        String code = "public class Main { \n" +
                "/*comm1*/ \n" +
                "someMethod(); \n" +
                "/*comm2*/ \n" +
                "}";
        assertEquals(3, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnThreeLinesForThreeLinedInputWithSeveralBlockCommentAndCodeLineBetween() {
        String code = "public class Main { \n" +
                "/*comm1*/ \n" +
                "someObject.method1().method2(); \n" +
                "/*comm2*/ \n" +
                "} \n";
        assertEquals(3, counter.getCodeLinesCount(code));
    }

    @Test
    public void shouldReturnThreeLinesForThreeLinedInputWithBlockCommentWithImitationsOfCommentsInside() {
        String code = "/* block comment //line comment imitation\n" +
                "                /* block comment imitation **/ \n" +
                "public class Main { \n" +
                "someObject.someMethod(); \n" +
                "}";
        assertEquals(3, counter.getCodeLinesCount(code));
    }

}